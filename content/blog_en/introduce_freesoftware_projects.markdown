---
title: An Answer to "How to introduce your project to the world?"
date: 2019-10-26 22:14:41 -06:00
categories: free software
path: introduce_freesoftware_projects
---

Originally published in [Dev.To](https://dev.to/mcsh/an-answer-to-how-to-introduce-your-project-to-the-world-1p07).

I was gonna write this as a comment for [this post](https://dev.to/dimensi0n/how-to-introduce-your-project-to-the-world-2i9n), but then it got too long and I decided to publish it here and then comment the link there!

You need to know that many open source projects are the child of big companies, and well, they get an unfair head start. But if you are starting to create your own project, here is a basic roadmap of what you need to do:

# Step One: What is it?

First of all, your project solves a problem, in the case of VueJS and React, it was designed to make frontend development easier and reusable. So what problem do you solve? Are you creating a library to do a task that is hard to implement, or is your project a boilerplate for writing CLI applications in Python? Figure this out.

Now you need to talk about this issue and address your solution(the project). Generally speaking, I see two different categories, problems people know they have and are looking to solve, or improvements that they don't know they need and you have to bring it to their attention.

If they are looking for solutions, and you just happen to have one, just show up and propose your answer. Do they ask on StackOverflow? Go there. Do they ask on a subreddit? A mailing list? You have to know your audience.

But what if they are ignorant, and you just created something that makes it a whole lot better? Well, it would be harder for you, I'm not gonna lie. But the formula is the same, write on a blog, in a Twitter rant, or in a subreddit. Talk about it in a YouTube video or a podcast. Ask your friends or colleagues to try it out. Spread the word!

# Step Two: Use your personal brand

Everyone has it. Do you have a twitter account with lots of followers? Use it to promote your project, and follow my [English account](http://twitter.com/sajjad_en) while you are there, it's new and it's empty, but I'm working on it :P

Write about it on your blog, post a video on YouTube, write as a guest for someone else's blog, talk about it on Reddit, on dev.to and other places. You have a voice, even if a small one, use it to let others know about your project. If they aren't aware of it, they won't use it.

# Step Three: Actually have a project

Now, this might sound crazy, but a small project that is almost a replica of the standard tutorials for a well-known library is not exactly useful. A project that is halfway done without any proper documentation is a no go as well, heck, even a complete project without proper documentation is useless.

So first ensure it works, then write all of its documents, then expect the users to show up, and go from there. 

# Step Four: Keep it going

There will always be a feature request. A project without a feature request is a dead one. Keep making it better, upgrade your dependencies regularly to prevent any security risk. Use a proper versioning system, and for the love of god keep backward compatibility as much as you can (looking at you Tensorflow!), even when bumping major versions, it's a good idea to not change so much.

# Step Five: Let others in

Any project you create is your baby. But being an asshole about it and keeping it all to yourself isn't a good way of going on. I'm not saying accept every - or even any - PR that comes your way or implementing every feature request, but do listen to what people want, and do look at the PR that they have sent, it might be useful.

# Step Six: Don't give up

It takes time. It really does. You won't wake up after a first commit to a thousand stars on Github, but don't give up. If you believe in what you have created, well, keep it for yourself and keep going. Maybe others will find it useful one day.
