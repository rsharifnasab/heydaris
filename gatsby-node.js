const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;

  let result = await graphql(`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog_en/"}}) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: "/en/blog/" + node.frontmatter.path,
      component: path.resolve(`src/templates/en_post.js`),
      context:{
        file: node.frontmatter.path,
      },
    });
  });

  result = await graphql(`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog_fa/"}}) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: "/fa/blog/" + node.frontmatter.path,
      component: path.resolve(`src/templates/fa_post.js`),
      context:{
        file: node.frontmatter.path,
      },
    });
  });

  result = await graphql(`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/wiki_fa/"}}) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    console.error(result.errors);
  }

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: "/fa/wiki/" + node.frontmatter.path,
      component: path.resolve(`src/templates/fa_wiki.js`),
      context:{
        file: node.frontmatter.path,
      },
    });
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  // highlight-next-line
  if (node.internal.type === `MarkdownRemark`) {
    let value = null;
    const p = node.frontmatter.path;
    if(/\/blog_fa\//.test(node.fileAbsolutePath)){
      value = "/fa/blog/"+ p;
    }
    if(/\/blog_en\//.test(node.fileAbsolutePath)){
      value = "/en/blog/"+ p;
    }
    if(value)
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
