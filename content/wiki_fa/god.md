---
title: خدا
path: god
---

خدا مبحث بسیار پیچیده‌ای هست، شاید برای همین باشه که تصمیم گرفتم اولین پست ویکی مربوط به خدا باشه. لینک مطالبی که در این باره نوشتم:

* [به بهانه خدا - مقدمه](http://heydaris.com/fa/god)
* [به بهانه خدا - گودل](http://heydaris.com/fa/godel)
* [به بهانه هیولای اسپاگتی - راسل](http://heydaris.com/fa/fsm-russel)
* [به بهانه خدا - هاوکینگ](http://heydaris.com/fa/hawking)

توییت‌های زیر هم در مورد خدا نوشته‌ام:

* [انسلم و اثبات خدا](https://twitter.com/Sajjad_Heydari/status/1220574537487659009)
* [علاقه من به کلمه خدا](https://twitter.com/Sajjad_Heydari/status/1220828195374731269)
