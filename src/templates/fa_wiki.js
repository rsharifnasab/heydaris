import React from "react";
import { graphql } from "gatsby";

import Layout from "../components/layout";

export default function Template({ data }) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;
  return (
    <Layout dir="rtl" pageTitle={frontmatter.title} prefix="true">
      <h1>{frontmatter.title}</h1>
      <div
        className="blog-post-content"
        dangerouslySetInnerHTML={{ __html: html }}
      />
    </Layout>
  );
}

export const pageQuery = graphql`
  query($file: String!) {
    markdownRemark(frontmatter: { path: { eq: $file } }, fileAbsolutePath: {regex: "/wiki_fa/"}) {
      html
      frontmatter {
        path
        title
      }
    }
  }
`;
