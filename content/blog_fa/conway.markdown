---
title: به بهانه خدا - کانوی
date: 2020-05-16 23:59:54 -06:00
categories: god logic
path: conway
---


قبل از هر چیزی  لازم می‌دونم متذکر شم که این مطلب تنها جمع‌آوری و بررسی منطقی مطالب است و لزوما بیانگر اعتقادات شخصی من - سجاد حیدری - نیست. متشکرم که توجه می‌کنید.

می‌تونید نویسه‌های قبلی سری به بهانه خدا را از لینک‌های زیر بخونید:

* [به بهانه خدا - مقدمه](https://heydaris.com/fa/blog/god)
* [به بهانه خدا - گودل](https://heydaris.com/fa/blog/godel)
* [به بهانه هیولای اسپاگتی - راسل](https://heydaris.com/fa/blog/fsm-russel)
* [به بهانه خدا - هاوکینگ](https://heydaris.com/fa/blog/hawking)

---

پروفسور جان کانوی ریاضی‌دان مطرح قرن بیستم و بیست و یکم بود. او تلاش داشت تا هر چیزی رو که تو واقعیت وجود داره توضیح بده، و برای توضیح این مفاهیم از ریاضیات استفاده می‌کرد. در موردشون گفتن که جذاب‌ترین روش‌های توضیح ریاضیات رو هم داشته، همیشه با خودش تاس و کارت‌های بازی و طناب و چیزهایی از این قبیل حمل می‌کرده تا به دیگران یا خودش مفاهیم ریاضی رو یادآوری کنه. 

پیشرفت‌های علمی‌ که داشتن باعث شد تا سال ۱۹۸۱ عضو «انجمن سلطنتی لندن برای پیشرفت دانش طبیعی» بشن. یکی از بزرگ‌ترین افتخاراتشون این بوده که در صفحات قبلی کتابی که برای این عضویت امضا کردن، اسامی کسانی مثل آیزاک نیوتون، آلبرت انیشتن، [آلن تورینگ](https://heydaris.com/fa/blog/turing_ai) و [برترند راسل](http://blog.heydaris.com/fa/god/logic/2018/12/03/FSM_Russell.html) بوده. او برای جوان ماندن ذهنش سعی می‌کرده اعداد بزرگ رو بر هم تقسیم کنه، عدد پی رو تا هزار و صد و یازده رقم حفظ کنه، از الگوریتم doomsday استفاده کنه تا بلادرنگ روز هفته هر تاریخی رو حساب کنه. همچنین وی الگوریتم‌های عجیبی هم خلق می‌کردند، مثلا براب شمردن تعداد پله‌ها هنگام بالا رفتن بدون این که شمارشی در کار باشه، یا الگوریتمی برای این که چطوری به سربع‌ترین حالت صفحات کلاسوری دو طرفه نوشته رو ورق زد و خوند. در مورد خودش گفته «من نَفْس بزرگی دارم، بارها گفته‌ام که بزرگترین عیب من فروتنیه، اگر فروتن نبودم کامل و بی عیب و نقص می‌شدم» همچنین بارها از کتاب اسکار وایلد نقل کرده که «زندگی خیلی مهم‌تر از اونه که جدی گرفته شه» با وجود این توضیحات شاید بیشتر با اخلاقیات ایشون آشنا بشید. متاسفانه پروفسور کانوی پس از ابتلا به covid-19 در سال ۲۰۲۰ از دنیا رفتند.

ایشون از ابزارهایی که در اختیار داشت استفاده کرد و مطالب جالبی رو در باره زندگی و اختیار اثبات کردن. تو این پست اما به دومورد اصلی اون‌ها می‌خواهیم بپردازیم، بازی زندگی کانوی و تئوری اختیار کانوی.

---

## بازی زندگی کانوی

بازی زندگی، بر خلاف اسمش، واقعا یک بازی نیست. در بهترین حالت یک بازی با صفر بازیکن هست و در دقیق‌ترین توصیف یک اتوماتای سلولی‌ست. مثل همه اتوماتاهای سلولی دیگه، تو این دنیا تعداد زیادی ماشین خیلی ساده داریم که مثل هم عمل می‌کنن و به همسایه‌هاشون وصلن. ماشین‌ها - یا سلول‌ها - روی یک صفحه شطرنجی چیده شدند، هر خونه دقیقا یک ماشین داره، و هر ماشین در هر لحظه یا خاموش هست یا روشن، یه به اصطلاح یا مرده‌است یا زنده. در هرگام ماشین‌ها به ۸ همسایه‌ای که دارند نگاه می‌کنند و:

* چنانچه سلول زنده کمتر از دو همسایه زنده داشته باشد، بر اثر کمبود جمعیت می‌میرد
* چنانچه سلول زنده دقیقا دو یا سه همسایه زنده داشته باشد، زنده می‌ماند
* چنانچه سلول زنده بیشتر از سه همسایه زنده داشته باشد، بر اثر ازدیاد جمعیت می‌میرد
* چنانچه سلولی مرده دقیقا سه همسایه زنده داشته باشد، براثر تولید مثل زنده می‌شود

این قوانین رو میشه به حالت دیگه بیان کرد:

* سلول‌های زنده‌ای که ۲ یا ۳ همسایه زنده دارند زنده می‌مانند
* سلول‌های مرده‌ای که دقیقا ۳ همسایه زنده دارند زنده می‌شوند
* تمام سلول‌های دیگر می‌میرند

حالت اولیه سیستم به اصطلاح seed نامیده میشه، و تنها دخالت بازیکن توی بازی هست. چند اتفاق جالب توی بازی زندگی کانوی می‌افته، یکی این که میشه پترن‌هایی درست کرد که تا ابد زنده بمونن و نامیرا باشن، همچنین میشه پترن‌هایی درست کرد که ماشین تورینگ رو شبیه سازی کنه،‌ یعنی قدرت محاسباتی این بازی،  برابر با قدرت محاسباتی کامپیوترهای امروزیه.

اما چیزی که این بازی رو جالب‌تر هم می‌کنه بحث تصمیم ناپذیر بودن اون هست. کانوی اثبات می‌کنه اگر ما حالت صفحه رو در زمان t داشته باشیم، هیچ راه کلی نیست که حالت صفحه رو در زمان t + a بدونیم بدون این که a بار صفحه رو آپدیت کنیم. دقت کنید که هر خونه فقط ۲ حالت داره و ما فقط ۳ تا قانون برای آپدیت کردن سیستممون داریم.  این ویژگی نشون میده که قوانین خیلی ساده می‌تونن محیط‌های پیچیده رو بسازن، بحثی که پروفسور ولفرام هم خیلی بهش توجه می‌کنه. برداشت کانوی از این اتفاق این بود که احتمالا دنیای ما هم قوانین بسیار ساده‌ای داره، و تکرار اون‌ها و کنار هم قرار گرفتن اون‌ها باعث به وجود اومدن دنیایی به این پیچدگی شده. علاوه بر این نشون میده که «پیش بینی‌آینده» بدون شبیه‌سازی اون عملا غیرممکنه، و از اونجایی که هر شبیه سازی‌ای داخل دنیا باید اجرا شه، سرعت اجراش هرگز نمی‌تونه از سرعت اجرای دنیا بیشتر بشه مگر این که کلی‌نگری کنه، که در اون صورت دقیق نیست. شاید این توضیحی باشه بر چرایی دقیق نبودن فرمول‌های فیزیک!

---

## تئوری اختیار کانوی

بازی زندگی کانوی جالب هست، اما چیز جدیدی نشون ما نمیده، ما از قبلتر می‌دونستیم که قوانین ساده می‌تونن پترن‌های پیچیده بسازن (اتوماتای سلولی ولفرام) و همچنین می‌دونستیم که دنیا رو نمیشه با سرعت بالا شبیه سازی کرد (مسئله halting ماشین‌های تورینگ)، اما کار دیگر کانوی، تئوری اختیارش، نتایج بسیار بسیار جالبی رو برای ما به ارمغان میاره. بحث جبر یا اختیار بیش از دو هزار ساله که فکر انسان‌ها رو به خودش مطرح کرده، مسئله این هست که آیا ما برای زندگیمون تصمیم می‌گیریم یا آیا تصمیم‌های ما از قبل گرفته شده و ما صرفا بازی‌گر این دنیا هستیم؟ بسیاری از فلاسفه این بحث رو باز کردند و پاسخ‌های خودشون رو دادند، اما کانوی به جای جواب دادن به این بحث، یک پدیده فیزیکی رو به ما معرفی می‌کنه.

کانوی این مسئله رو اینطور تعریف می‌کنه، اگر یک آزمایش شونده با یک تصمیم روبرو شود، آیا با دانستن تمام گذشته او می‌توان تصمیمش را حدس زد؟ اگر بشود جواب «جبر» هست و اگر نشود «اختیار». تعریف ساده و واضحی هم هست. کانوی در ادامه ۳ پیش‌فرض رو در نظر می‌گیره:

* سرعت انتقال اطلاعات یک ماکسیمال دارد،
* مجذور اسپین هر ذره کوانتومی جایگشتی از ۰ و ۱ و ۱ هست،
* درهم تنیدگی کوانتومی وجود دارد.

این پیش‌فرض‌ها تماما در فیزیک کوانتومی پذیرفته شده هستند، کانوی سپس در یک مقاله که در سال ۲۰۰۶ منتشر شده، با ریاضیات و فیزیک اثبات می‌کنه که اگر در اون آزمایش، مورد آزمایش اختیار داشته باشه، آنگاه ذرات بنیادی هم اختیار دارند. قشنگی این اثبات، درست مثل قشنگی اثبات گودل، اینه که اگر شما جبر رو باور دارید، تاییدی بر باور شما می‌ذاره (ذرات بنیادی هم جبر برشون هست) و اگر اختیار رو باور دارید، تاییدی بر باورشما می‌ذاره.

این تئوری بازخوردهای جالبی داشت، مقاله‌ای که در سال ۲۰۱۴ در Foundations of Physics منتشر شد بیان می‌کنه که کانوی و کوهن (co-author این تئوری) اثبات کردن که جبر با تعدادی از فرض‌های پیشین فیزیک در تضاد هست، همچنین قاضی دیوید هادجسن، فیلسوف استرالیایی در مقاله‌ای که در انتشارات آکسفورد چاپ شده گفته علم امروز به ظاهر با جبر مخالف است. اما با وجود این دو نظر ظاهرا مطرح و پذیرفته شده در جامعه آکادمیک، تعداد زیادی از جامعه ریاضی‌دانان آمریکا شامل شلدون گلداشتاین و دنیل تاسک بر این باورند که این تئوری نه تنها جبر رو رد نمی‌کنه، بلکه با اختیار در تضاد کامل هست. نکته جالب این قضیه اینه که تمام این نظرات در بالاترین ژورنال‌های روز چاپ شدند.

کار کانوی در واقع نسخه‌ای بهتر از تئوری بل هست، بر اساس این تئوری فیزیک کوانتومی با نظریه متغییرهای نهان در تضاد هست. اگر یادتون باشه در به بهانه‌خدای هاوکینگ گفتیم که انیشتن باور داشت که خدا تاس بازی نمی‌کند و احتمالات در فیزیک کوانتومی بی‌معناست، در واقع انیشتن می‌گفت که فیزیک کوانتومی حتما یک یا چند ویژگی فیزیکی رو در نظر نگرفته که بعدا کشف می‌شن و نبود اون‌ها در معادله باعث احتمالاتی شدنشون میشه. این نظریه به اسم متغییرهای نهان مطرح هست. تئوری بل اما این نظریه رو رد می‌کنه و اثبات می‌کنه در صورت وجود حتا یک متغییر نهان، فیزیک کوانتومی ویژگی‌های خودش رو از دست میده. این حرف رو میشه به این صورت برداشت کرد که ذرات کوانتومی یا خودشون تصمیم می‌گیرن که چه اسپینی رو بگیرند (اختیار) یا از جای دیگر بهشون الهام شده، همون چیزی که هاوکینگ بهش می‌گفت دخالت خدا در دنیا یا معجزه.

تئوری بل اما پیش‌فرض‌های زیادی داره، از احتمالات استفاده می‌کنه و اثبات سنگینی داره،‌ اما تئوری اختیار تنها ۳ فرض داره که تمام اون‌ها با قوانین فیزیک خوانا هستند، و اثبات نسبتا ساده‌ای هم داره. به این خاطر میگیم که تئوری اختیار کانوی نسخه بهتری از تئوری بل هست. اما کانوی در سال ۲۰۰۹، سه سال پس از انتشار اولین نسخه از مقاله‌اش، اثبات بهتری از اون رو ارائه داد که در اون دیگر نیازی نیست سرعت انتقال همه اطلاعات ماکسیمال داشته باشد، و فقط کافیست بین دو ذره که در فضا فاصله دارند بر سر یک «تصمیم» این ماکسیمال سرعت وجود داشته باشد. کوهن در سال ۲۰۱۷، پس از ۸ سال، «قضیه قوی‌تر اختیار کانوی» رو پذیرفت.

اما این تئوری چه ربطی به «به بهانه خدا» داره؟ جدا از زیبایی این نظریه و بحث‌های جذاب پیرامون اختیار و جبر، این نظریه دوباره به چیزی تاکید می‌کنه که توی اولین پست بهش اشاره کردیم، اثبات چیزهایی که در ریاضیات مدل درستی ندارند نشدنی هست و این باورها قلبیست، شما اگر به آن‌ها باور دارید هر اثباتی تاییدی برای باورتان هست و اگر باور ندارید هر ردی تاییدی بر عدم باورتان. کانوی به خوبی این قضیه رو درک کرده بود و به جای اثبات وجود یا عدم وجود اختیار، ثابت کرد که اگر اختیار وجود داشته باشد ذرات سازنده هم اون‌ها رو دارند.

نظر شما چیه؟ به نظرتون ما - و ذرات سازنده - اختیار داریم، یا با جبر پیش می‌رویم؟ می‌تونید برای بحث بیشتر به [اینجا](https://t.me/pi_developer_discuss) ملحق شوید.

