import React from "react";
import {graphql} from "gatsby";

import Layout from "../../components/layout";
import PostLink from "../../components/post_link";
import PostList from "../../components/post_list";

export default ({data}) => (
  <Layout dir="rtl">
    <h2>Posts</h2>
    {console.log(data)}
    <PostList>
      {data.allMarkdownRemark.edges.map(({node})=>(
        <PostLink link={"/fa/wiki/"+node.frontmatter.path}>
          {node.frontmatter.title}
        </PostLink>
      ))}
    </PostList>
  </Layout>
);

export const query = graphql`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/wiki_fa/"}}) {
        edges {
          node {
            frontmatter {
              path
              title
            }
          }
        }
      }
    }
`;
