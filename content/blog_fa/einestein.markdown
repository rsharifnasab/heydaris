---
title: به بهانه خدا - انیشتین
date: 2022-03-03 05:21:10 -06:00
categories: god logic
path: einestein
---


قبل از هر چیزی لازم می‌دونم متذکر شم که این مطلب تنها جمع‌آوری و بررسی منطقی مطالب است و لزوما بیانگر اعتقادات شخصی من - سجاد حیدری - نیست. متشکرم که توجه می‌کنید.

می‌تونید نویسه‌های قبلی سری به بهانه خدا را از لینک‌های زیر بخوانید:

* [به بهانه خدا - مقدمه](https://heydaris.com/fa/blog/god)
* [به بهانه خدا - گودل](https://heydaris.com/fa/blog/godel)
* [به بهانه هیولای اسپاگتی - راسل](https://heydaris.com/fa/blog/fsm-russel/)
* [به بهانه خدا - هاوکینگ](https://heydaris.com/fa/blog/hawking)
* [به بهانه خدا - کانوی](https://heydaris.com/fa/blog/conway)

---

در مورد باورها و زندگی آلبرت انیشتین کتاب‌ها نوشته شده و بررسی همه اون‌ها ممکن نیست. در این نوشته سعی می‌کنیم چند باور مستند او را مورد بررسی قرار بدیم. برخلاف میلم بخش زیادی از این نوشته رو حذف کردم چرا که برای بسیار از ادعاها منبع معتبر پیدا نکردم.

آلبرت اینشتین، فیزیک‌دان معاصر که نظریه نسبیت را به جهان معرفی کرد زندگی پر پیچ و خمی داشته. علاوه بر فعالیت‌های علمی، در زندگیش به گروه‌های بشردوستانانه‌ی زیادی هم خدمت کرده بود که تعداد زیادی از آن‌ها در راستای مقابله با جنگ و استفاده از شکافت هسته‌ای به عنوان سلاح بود. صحبت درباب زندگی پر پیچ و خم انیشتین خالی از لطف نیست، اما تکرار مکررات است و به همین بسنده کنیم که انیشتین از ابتدای زندگیش با عقاید و باورهای مختلف در تماس بوده، از تعالیم ابراهیمی مثل یهودیت و مسیحیت تا باورهای بودایی، اما به گفته خودش یک ندانم‌گراست که به وجود خدای فردی - خدایی که در زندگی انسان‌ها دخالت می‌کند - باور ندارد.

## خدای اسپینوزا

یکی از باورهای منتسب به انیشتین اعتقاد به اسپینوزیسم هست که دسته‌ای از پانتئیسم هست. این دیدگاه باور دارد که مفهوم خدا برابر با تمام جهان هستی هست، باوری که میگه خدا همه چیز است و همه چیز خداست. اسپینوزا مفهوم رو کمی فراتر هم می‌بره و مدعی میشه که اصل این مفهوم برای انسان غیرقابل درک هست، و ما فقط توان نزدیک شدن به بخشی از حقیقت رو داریم. اسپینوزا در جواب به «خدا چی هست؟» میگه سه چیز، جوهر، ویژگی و وجوه آن‌ها. جوهر خود همان چیز است، مستقل از هرگونه برداشت و اندازه گیری و نگاه. ویژگی چیزهایی هست که مستقل از جوهر و ماده وجود ندارن و با تکیه بر اون‌ها، با مشاهده اون‌ها، به وجود میان و در نهایت وجوه حالت‌های مختلف یک چیز هستن. برای مثال، اگر آب جوهر باشد، به خودی خود وجود دارد وجود داشتنش به چیز دیگری احتیاج ندارد، بو و رنگ نداشتن اما وجوه آب هستن و ویژگی‌های آب چیزهاییست که ذهن ما به آن نسبت می‌دهند، مثل خیسی و یا خنکی. اسپینوزا باور داره که وجوه خدا نه تنها نامتناهی هستند، که درک بسیاری از آن‌ها برای ما امکان پذیر نیست. به گفته خودش «جهان صحنه خیمه شب‌بازی نیست و خدا خیمه‌شب باز نیست که آن را کنترل کند و هر گاه لزوم داشت قوانین طبیعت را نقض کند و زیر پا بگذارد و بعد دوباره آن‌ها را به کار گیرد، خدا همان طبیعت است که در برگیرنده تمام جوهرهاست.»

رویکرد انیشتین در مورد خدای اسپینوزا در بازه‌های مختلف متفاوت بوده، گاها گفته که من به پانتئیسم باور ندارم و گاهی گفته که عقاید اسپینوزا برایم بسیار نزدیک به حقیقت هستن. اما شاید بهترین منبع منتسب به او در این باره این سخن باشه که «جهان هستی برای ما مثل کودکیست که وارد کتابخانه‌ای می‌شود که کتاب‌های زیادی داره به زبان‌های مختلف. این کودک می‌دونه که کسی این کتاب‌ها رو نوشته و کسی اون‌ها رو چیده اما نمی‌تونه نظم موجود در کتابخونه رو بفهمه، نمی‌تونه همه کتاب‌ها رو بخونه، اما تو ذهن خودش ممکنه حدسی بزنه از نظمی که اونجا وجود داره. این حدس همون برداشت ما از خداست. ما دنیا رو پر از نظم می‌بینیم ولی نمی‌تونیم همه اون رو درک کنیم... اسپینوزا بزرگترین فیلسوف مدرن هست، به این خاطر که اولین فیلسوفی هست که با این نگاه به هستی روح و جسم رو از هم جدا نمی‌کنه و اون‌ها، با علم به درک نکردن چگونگی اتحادشون، واحد می‌بینه.»

## جبر و اختیار

(همونطوری که توی نویسه‌های کانوی و هاوکینگ گفتیم) انیشتین باور داشته که دنیا تحت کنترل جبر هست. از یکی بودن جسم و روح و ثابت بودن قوانین فیزیک نتیجه گیری می‌کنه که تصمیم‌های انسان از خودش نیست و نتیجه قوانین ساده فیزیک ذرات هستن. او همچنین اعتقاد داشت که «خدا تاس نمی‌ریزد» و بنابراین با زیر سوال بردن جنبه احتمالاتی نظریه کوانتوم، اختیار رو هم خارج از توان انسان می‌دونه. توصیه می‌کنم برای بحث کامل‌تر پیرامون این قضیه [به بهانه خدا - کانوی](https://heydaris.com/fa/blog/conway) رو بخونید.
