#!/bin/bash

NAME=_drafts/$1.markdown
touch $NAME

p(){
    echo $@ >> $NAME
}

p ---
p title: $1
p date: $(date -Is | awk 'gsub("T", " ")' | awk 'gsub("-05:00", " -06:00")') 
p categories: 
p path: $1
p ---
p

echo $NAME
