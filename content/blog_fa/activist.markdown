---
title: چگونه یک اکتیویست نباشیم
date: 2021-03-28 23:49:56 -06:00
categories:
path: activist
---

تا حالا شده انقدر به یک موضوع، هر موضوعی، حساس باشید که بخواید همه دنیا رو نسبت بهش آگاه کنید؟ ممکنه یک راه جدید سالاد خوردن باشه، یا نظری درباره آزاد کردن ماریجوانا (مثبت یا منفی، من قضاوتتون نمی‌کنم)، یا حتا یک ساختار جدید تحصیلی برای دانش‌آموزان. هر موضوعی که هست، برای شما مهمه، و همین کافیه.

سوال اینجاست که چطور باید یک اکتیویست باشیم و به همه دنیا - یا به همسایه‌هامون - آگاهی بدیم از نظراتمون؟

از اونجایی که اکتیویست بودن مسئله بسیار پیچیده‌ای هست، بیاید به جاش اکتیویست نبودن رو بررسی کنیم و ببینم چطور باید یک اکتیویست نباشیم؟

## گام صفرم، موضع نگیرید

برای اکتیویست نبودن کافیه بتونید موضع نگیرید. اگر مسائل براتون بی‌اهمیت هستن تبریک می‌گم، شما اکتیویست نیستید.

همونطوری که تو مقدمه گفتیم، هدف اکتیویست بودن در وهله اول آگاه کردن جامعه از یک نگرش جدیده، این هدف به مرور زمان از آگاه کردن می‌تونه تبدیل به تغییر ایجاد کردن بشه، ولی قبل از شروع باید مشخص کنیم که آگاهی رو برای چه موضوع می‌خوایم ایجاد کنیم؟

## گام اول، آماده نباشید

متاسفانه برای ما آدم‌ها مسائل مختلف اهمیت دارن، و بی‌اهمیت بودن به همه چیز برامون راحت نیست. این باعث میشه خیلی از آدم‌ها از فیلتر مرحله صفرم رد بشن، اما جای نگرانی نیست. اگر درست تحقیق نکنیم و اطلاعات کافی نداشته باشیم دیگه اکتیویست نیستیم.

تصور کنید وسط یک بحث پر تنش - مثلا درباره مسئله فلسطین و اسرائیل - هستید. بحث رو شما شروع کردید و طرف مقابل در باره وقایع جنگ جهانی دوم صحبت می‌کنه. اگر شما به اندازه کافی مطالعه نکرده باشید و آماده نباشید، ممکنه نتونید به خوبی استدلال کنید و فعالیت شما قبل از شروع با شکست روبرو میشه. شما باید در مورد اون موضوع و مسائلی که در حاشیه‌اش هستن مطالعه کنید و آماده بحث باشید که بتونید ازش دفاع کنید و در مورد حرف بزنید.

## گام دوم، متمدن نباشید

مستقل از میزان آگاهیتون به مسائلی که براتون مهمه، برای اکتیویست نبودن کافیه متمدن نباشید. اگر به جای گفت‌و‌گو برای آگاهی بخشی به ترسوندن مخالفانتون رو بیارید، اگر به جای معرفی مزیت روشتون، روشتون رو به زور به مردم غالب کنید، خیلی راحت از یک اکتیویست به یک تروریست (حالا شاید نه به این شدت) تبدیل می‌شوید.

بدترین تبلیغ منفی برای هر مسئله، رفتار نامتمدنانه از طرف علاقه‌مندانش هست. مستقل از این که پیام شما چیه، اگر با خشونت و عصبانیت اون رو منتقل کنید نه تنها مخاطبینتون نسبت بهش جبهه می‌گیرن، که مخالفین هم از رفتار شما سواستفاده می‌کنن تا نظر مردم رو بر علیهتون کنند. 

لازم به ذکره که در بعضی شرایط اعتراض نامسالمت آمیز ممکنه تنها راه باشه، اما اگر آخرین راحتون نیست برای پیشرفت اهدافتون تجدید نظر کنید.

## گام سوم، نظراتون رو برای خودتون نگه‌دارید

اگر تا اینجا موفق نبودیم که نظر تشکیل ندیم و متمدن نباشیم و شرایط ما رو به سمتی می‌برد که اکتیویست بشیم، بهترین راه اکتیویست نشدن اینه که نظرمون رو برای خودمون نگه‌داریم. اگه درباره نظرمون صحبت نکنیم، هدف اول که آگاهی بخشی هست با شکست روبرو میشه.

صحبت کردن در مورد نظرات لزوما راحت نیستند، اما گروه‌های مختلف در طی زمان روش‌های مختلفی برای صحبت کردن پیدا کردند. از چاپ کردن اون‌ها در رزونامه‌ها و درست کردن پوسترهای تبلیغاتی، تا مصاحبه و سخنرانی، روش‌های مختلفی رو براتون فراهم می‌کنن تا بتونید در مورد نظرتون صحبت کنید. اما مزیتی که اکتیویست‌های قرن بیست و یکم به اکتیویست‌های گذشته دارن اینه که اینترنت و شبکه‌های مجازی رو در اختیار شما قرار می‌دن تا به راحتی - و با هزینه کم - نظرتون رو با بقیه به اشتراک بگذارید.

## گام چهارم، نظر مخاطب رو جلب کنید

اگر گام سوم هم مانع شما برای اکتیویست شدن نشد، شاید صحبت کردن برای یک اتاق خالی جواب شما باشه. نداشتن مخاطب باعث میشه یک اکتیویست که با هیجان درباره روش جدید سالاد خوردن حرف می‌زنه تبدیل به یک دیوانه شه که بلند بلند با خودش حرف می‌زنه.

مشکل اینجاست که حرفی برای گفتن داشتن تضمینی برای شنیده شدن نیست. جذب کردن نظر مخاطب شاید سخت‌ترین کار یک اکتیویست باشه، مخصوصا اگر بودجه محدودی داشته باشه. گروه‌های بزرگ و آدم‌های مطرح برای به اشتراک گذاشتن نظرشون می‌تونن پول یا اعتبار خرج کنن، تبلیغات و مصاحبه با خبرگذاری‌ها کار راحتی براشون هست، اما برای ما آدم‌های عادی این فرایند بسیار سخت‌تر هست. در طول تاریخ اکتیویست‌های مختلف روش‌های مختلفی برای جلب کردن نظر مخاطب به کار گرفته‌اند، از ایجاد شایعه حضور یک بند معروف در مراسم یا دادن گل به سربازهای از جنگ برگشته، از فراهم کردن نوشیدنی و اغذیه رایگان یا پوشیدن لباس‌های عجیب و غریب، و هزاران کار دیگر ابزارهایی بودن که گروه‌های مختلف برای جلب کردن نظر مخاطب استفاده کردن که شاید در دنیای امروز هم بشه اون‌ها رو انجام داد.

## گام پنجم، نظر مخالف رو نشنوید

اگه نظر مخاطب به حرف‌های شما جلب شد، برای اکتیویست نشدن کافیه فقط و فقط حرف‌های خودتون رو طوطی‌وار تکرار کنید و به هیچ نظر دیگه‌ای اهمیت ندید. اگه بتونید به یک نوارکاست تبدیل شید، دیگه اکتیویست نیستید.

شنیدن نظر مخالف می‌تونه به شما کمک کنه که ضعف رویکردتون رو ببینید. شاید دلیل مخالفتشون با شما برداشت اشتباهی باشه که با تغییر حرفتون درست شه، شاید هم اینطور نباشه و با ذات رویکردتون مشکل دارن. حداقل کمکی که شنیدن این نظرات به شما می‌کنه اینه که بهتون فرصت میده خودتون رو براشون آماده کنید. اگر قبل از یک مناظره - که یکی از مراحل تغییر هست - بتونید این نظرات رو بشنوید راحت‌تر بهشون جواب میدید.

سختی این کار اینه که پیدا کردن اون نظرها راحت نیست. معمولا آدم‌های دور و برمون نظرات یکسانی با ما دارن، یا حتا اگر نظر مخالفی داشته باشن ممکنه بهمون نگن. پس مهمه که بتونیم آدم‌هایی رو پیدا کنیم که نظر یکسانی باهامون ندارن و حرفاشون رو - بدون تلاش برای تغییر نظرشون - بشنویم.

## گام ششم، به صورت فعال دنبال تغییر نباشید

عصبانی بودن در شبکه‌های اجتماعی کافیه! هر وقت فرصتی پیش اومد توییت‌های دیگران رو ریتوییت کنید و عکس‌های بقیه رو لایک کنید. اینطوری هم خیال برتون می‌داره که مفید بودید، هم یه اکتیویست واقعی نمیشید.

آگاهی بخشی و تغییر یک شبه صورت نمی‌گیره، و بستگی به موضوعی که براتون مهمه ممکنه نیاز به دوندگی‌های زیادی هم داشته باشه. شما باید آماده باشید تا درباره اون موضوع صحبت کنید، آدم‌هایی رو پیدا کنید که نظر مشابهی به شما دارن، با هم هماهنگ بشید و با افرادی که می‌تونن اثر بگذارن نامه‌نگاری کنید، و حتا در صورت نیاز رو به فعالیت‌های گروهی دیگری برای جلب توجه بیارید.

## گام آخر

اگر تا اینجای کار هیچ گامی جلوی شما رو نگرفت، متاسفم، شما یک اکتیویست هستید. دلسرد نشید و ادامه بدید، یا اگر دلسرد شدید هم سعی کنید دوباره شروع کنید. دنیای ما کامل نیست، ولی انتخاب دیگری هم نداریم و تنها کاری که از دستمون بر میاد تلاش برای بهتر کردنش هست. شاید ما نتونیم ذات دنیا رو عوض کنیم، اما حتما می‌تونیم اون رو کمی بهتر کنیم.

براتون آرزوی موفقیت می‌کنم و شما رو دعوت می‌کنم تا با عضو شدن در [این کانال](https://t.me/pi_developer) و [این گروه](https://t.me/pi_developer_discuss) گامی رو در جهت پیدا کردن هم صحبت بردارید.