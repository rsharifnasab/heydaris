---
title: Turing and AI
date: 2020-04-23 16:26:20 -06:00
categories: AI
path: turings_ai
---

Turing defines an intelligent machine as one that can cause doubt in the human perception of it. Contrary to what it seems, this experiment is not a real measure of intelligence, it's more like a pseudoscience. What makes the judge doubt who is the machine and who is the human is not based on knowledge or processing power, but instead on things like the delay between sending messages or remembering the topic at hand. I'm not saying that is an easy task, but it's not as hard as it sounds.

This experiment might be more of a data-gathering / processing kind than an AI experiment since the machine needs to minimize the number of unknown topics that the user might talk about, and the most straight forward method seems to be that of increasing the known topics. Although some other machines employ different techniques for handling this task, for example, ELIZA, that claims to be a psychologist, keeps converting her inputs to questions and then asks them back from the owner. This might sound funny and not so intelligent, but the 56-year-old AI does a surprisingly good job. But does this mean that ELIZA - [which is written in less than 2 thousand lines of lisp](https://github.com/emacs-mirror/emacs/blob/d0e2a341dd9a9a365fd311748df024ecb25b70ec/lisp/play/doctor.el)- is smart? I don't think so.

But Turing wasn't that far from reality, what separates humans from animals is the power to tell stories, a process in which the storyteller must imagine a made-up world and live and make decisions as each and every one of the creatures in the said world, and then tell it in an entertaining way. All tasks that us humans start doing at an early age of 2 or 3. Also, it doesn't have to be a complete story every time, telling a lie is a form of storytelling too, we imagine a made-up world where things happened in an order that caused the vase to be broken without us being responsible for it.

It might make more sense to say that a machine is intelligent if it can tell a story that entertains humans. But how can we measure it? We've previously talked about the fact that part of the story is the medium (p.s: This article was in Persian and I have not translated it yet, sorry :P), so maybe the measuring should depend on the medium. In the Turing experiment, the medium is a chat in a terminal, whereas a story could be said in any medium, from a multiplayer game to an interactive story in a book, and sometimes the medium makes the storyteller jobs extremely easier.

With all that, how can we measure the story teller's creativity?
