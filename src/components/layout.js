import React from "react";
import {Link} from "gatsby";
import {Helmet} from "react-helmet";

import Github from "../assets/github.svg";
import Telegram from "../assets/telegram.svg";
import Twitter from "../assets/twitter.svg";
import Mastodon from "../assets/mastodon.svg";
import RSS from "../assets/rss.svg";

import styles from './layout.module.css';

export default ({children, dir="ltr", pageTitle, prefix=false}) =>{
  let title = dir==="ltr"? "Sajjad" : "سجاد";
  let home = dir==="ltr"?"/en":"/fa";
  let other = dir==="ltr"?"/fa":"/en";
  let other_title = dir==="ltr"?"فارسی":"English";
  let foot_note = dir==="ltr"?`Hi! This is Sajjad's blog, a CS student, a Geek! Welcome to my blog!`:`سلام! من سجاد هستم. دانشجوی علوم کامپیوتر،‌ یک گیک. به وبلاگ شخصی من خوش آمدید!`;
  let foot_title = dir==="ltr"?`Sajjad 'MCSH' Heydari`:`سجاد`;
  let blog_link = dir==="ltr"?"/en/blog":"/fa/blog";
  let rss_link = dir==="ltr"?"/rss.xml":"/rss_fa.xml";
  let blog = dir==="ltr"?"Blog":"وبلاگ";
  if(!pageTitle)
    pageTitle = title;
  if(prefix){
    let prefix_url = "https://heydaris.com"
    home = prefix_url + home;
    home = <a href={home} className={styles.site_title}>{title}</a>
    other = prefix_url + other;
    other = <a href={other} className={styles.other_title}>{other_title}</a>;
    blog_link = prefix_url + blog_link;
    blog_link = <a href={blog_link} className={styles.blog}>{blog}</a>;
  } else {
    home = <Link to={home} className={styles.site_title}>{title}</Link>;
    other = <Link to={other} className={styles.other_title}>{other_title}</Link>;
    blog_link = <Link to={blog_link} className={styles.blog}>{blog}</Link>;
  }
  return (
    <div dir={dir}>
      <Helmet>
        <meta charSet="utf-8"/>
          <title>{pageTitle}</title>
        {dir==="rtl"?
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vazir-font/27.2.1/font-face.css" integrity="sha512-ZHFuHiK3EA1uh2tx7nB0j9HyXR/IAFW24KVNFGjY8QIjtDKHmcowjUyObXF40wYrG25+kECHEbH8rL+HbvRwYA==" crossorigin="anonymous" />
        :""}
      </Helmet>
      <header className={styles.header}>
        <div className={styles.wrapper}>
          {home}
          {blog_link}
          <nav className={styles.site_nav}>
            {other}
          </nav>
        </div>
      </header>
      <main className={styles.layout} dir={dir}>
        {children}
      </main>
      <footer className={styles.footer}>
        <div className={styles.footer_wrapper}>
          <h2>{title}</h2>
          <div className={styles.footer_col_wrapper}>
            <div className={styles.footer_col1+ ' ' + styles.footer_col_mobile}>
              <ul className={styles.foot_list}>
                <li>{foot_title}</li>
                <li><a className={styles.foot_email} href="mailto:MCSHemail@gmail.com">MCSHemail@gmail.com</a></li>
              </ul>
            </div>
            <div className={styles.footer_col2+ ' ' + styles.footer_col_mobile}>
              <ul className={styles.foot_list}>
                <li className={styles.li}>
                  <a href="https://github.com/MCSH" className={styles.a}>
                    <Github className={styles.svg_icon}/>
                    <span className={styles.foot_username}>MCSH</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href="https://twitter.com/sajjad_heydari" className={styles.a}>
                    <Twitter className={styles.svg_icon}/>
                    <span className={styles.foot_username}>Sajjad_Heydari</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href="https://t.me/pi_developer" className={styles.a}>
                    <Telegram className={styles.svg_icon}/>
                    <span className={styles.foot_username}>pi_developer</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href="https://mastodon.social/@sajjad_heydari" className={styles.a} rel="me" >
                    <Mastodon className={styles.svg_icon}/>
                    <span className={styles.foot_username}>Sajjad_Heydari@mastodon.social</span>
                  </a>
                </li>
                <li className={styles.li}>
                  <a href={rss_link} rel="alternate" type="application/rss+xml">
                    <RSS className={styles.svg_icon}/>
                    <span className={styles.foot_username}>RSS</span>
                  </a>
                </li>
              </ul>
            </div>
            <div className={styles.footer_col3+ ' ' + styles.footer_col_mobile}>
              <p>
                {foot_note}
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
