module.exports = {
  siteMetadata:{
    siteUrl: 'https://heydaris.com',
    title: 'heydaris',
    description: '',
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "G-WE6QTKQRKY",
        ],
        pluginConfig: {
          head: true,
        },
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-remark',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blog_en',
        path: `${__dirname}/content/blog_en`
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blog_fa',
        path: `${__dirname}/content/blog_fa`
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'wiki_fa',
        path: `${__dirname}/content/wiki_fa`,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
      }
        `,
        feeds: [
          {
            /* highlight-start */
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map(edge => {
                /* highlight-end */
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  custom_elements: [{ "content:encoded": edge.node.html }],
                })
              })
            },
            query: `
        {
          allMarkdownRemark(
            sort: { order: DESC, fields: [frontmatter___date] },
            filter: {fileAbsolutePath: {regex: "/blog_en/"}},
          ) {
            edges {
              node {
                excerpt
                html
                fields { slug }
                frontmatter {
                  title
                  date
                }
              }
            }
          }
        }
        `,
            output: "/rss.xml",
            title: "Heydaris",
          },
          {
            /* highlight-start */
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map(edge => {
                /* highlight-end */
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  custom_elements: [{ "content:encoded": edge.node.html }],
                })
              })
            },
            query: `
        {
          allMarkdownRemark(
            sort: { order: DESC, fields: [frontmatter___date] },
            filter: {fileAbsolutePath: {regex: "/blog_fa/"}},
          ) {
            edges {
              node {
                excerpt
                html
                fields { slug }
                frontmatter {
                  title
                  date
                }
              }
            }
          }
        }
        `,
            output: "/rss_fa.xml",
            title: "سجاد",
          },
        ],
      },
    },
  ],
};
