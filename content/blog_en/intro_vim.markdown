---
title: Switching to VIM
date: 2020-02-05 09:33:42 -06:00
categories: vim
path: intro_vim
---

If you are reading this, chances are you've used a text editor before. You might have used Notepad on your windows machine, Textedit on a mac or maybe some version of gedit on a Linux box. You might have used a complicated editor, such as Word, one in your browser, like Google Docs, or even a simple one on your smartphone. If you are reading this, you have used an editor before. Some editors give you many features, they provide you with ways to configure fonts, embed photos, share them with your co-workers or even print them. On the other hand, some editors are just, well, editors. We call them What You See Is What You Get or WYSIWYG for short, these editors don't try to understand your document or display it in a fancy manner, they just do their job and open the file. This makes them great tools for programmers and sysadmins and others who just need to write. But not all editors are equal.

A few decades ago, when computers were really slow, having an editor such as Notepad would be a pain to use, remember that you probably don't have a mouse and that each keypress takes a good chunk of a second to be processed, so keeping your hand on the right arrow key wouldn't be of much help! In those days, a few editors came that solved these problems and surprisingly they are still here, one of them being vi.

Vi is a screen-oriented text editor which had a state mindset that allowed users to do what they want easier. It has a hard learning curve, but it speeds up your writing process by a lot. Vim is short for vi IMproved, which is the defacto standard editor on virtually all systems nowadays. In this post, we'll look at the why, and the how of starting using vim.

## Why

If you are a developer or a sysadmin, chances are you have to edit files over a network on systems that you only have ssh access to. So how are you going to edit the config files? Are you going to copy them over using scp or ftp every time? I don't think so.

Aside from that, you'll have to run an IDE like PyCharm or Visual Studio, which requires lots of processing power from your machine and maybe you rather keep that memory for your other ram hungry applications (looking at you Slack and Chrome). So you'll want to use a simple editor that gets the job done, but a basic editor is hard to use.

Enter vim. It's small and fast and doesn't have a big footprint on your machine. It's customizable so you can implement the things you miss from your IDE, and it's installed on virtually all machines. Plus, once you get the hang of it, you'll never want to go back. And oh, it's completely free!

## How

So, you want to use vim, but you have no idea how to begin. You've heard scary stories of people who started vim but were never able to leave this place, and you're just afraid. I don't blame you, I was too, but then I got sucked in and I loved it. To the point that I even install it inside emacs (using Evil) and Chrome (using cvim) and virtually everywhere else that I can.

First, understand how vim works. Every moment vim is in a state, it could be normal, insert, replace, visual or any other thing. Each state defines different behavior for each key, for example in insert mode, pressing the 'i' key will insert the char 'i' below the cursor, while in 'normal' mode the 'i' key will put you in insert mode. The only key that you really need to know is Esc, that key brings you back to the normal state, which as the name suggests is the basic state.

So how are you going to learn the other keys? Run vimtutor, and work through it a couple of times. You don't need to memorize every key and every combination, just enough to let you work. Learn insert mode, opening a file, writing (saving) and of course navigating. You can always look up other pieces as time goes by. Don't rush it. I've been using vim for years but I still don't know every key.

Next, decide which version of vim you want to use. Your options are Vi, Vim and (my personal favorite) NeoVim. Vi is old, and if you dig on any modern Linux distro, you'll see that the vi binary is just a link to vim. Vim is the standard, supported by Bram Moolenaar, and is pretty awesome. Neovim is a new fork of vim, well it's been around for half a decade now, that tried to change something in vim, like adding support for writing plugins in any language or changing the ownership from one person to a group of people. Look at their website to get a better idea, choose which version you want to use and just go with it.

Now that you have your setup and know your way around vim, it's time to configure it. Vim's configuration file is `~/.vimrc` while neovim is `~/.config/nvim/init.vim`, open (or create) the file and let's get to work.

### Show Line Numbers

Most programmers enjoy having a line number indicator in their program. To add line numbers to your vim configuration, add these lines to your config:

```
" Line Numbers
set number
```

Alright, this will show the numbers starting from 1. But what if we want to show lines in relative to our cursor? The variable for that one is called `relativenumber`. What I like to do is have the number variable set by default, and define a key that toggles relativenumber, so when I need to see the line numbers relative to the cursor, I can, and then I can go back. To do this, add this function to your config:

```
function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber
  else
    set relativenumber
  endif
endfunc
```

And map the F2 key to that:

```
nnoremap <F2> :call NumberToggle()<cr>
```

Now close vim and open it again, press F2 to see the numbers change.

### Basic Highlights

Highlighting in vim depends on your language, but no matter which one you use, there are common configurations that will help you. First, let's set the tab size:

```
set shiftwidth=4
```

This will tell vim to show each Tab as 4 spaces. Now I'm not gonna get into the whole tab vs space things, but if you prefer to have your tabs changed to space, add this line:

```
set expandtab
```

Now for a basic syntax highlighting, add these files:

```
" Enable syntax highlighting for supported languages
syntax enable

" Show matching parentheses, etc
set showmatch

" Specific actions for specific file types
filetype on
filetype indent on
filetype plugin on
```

Finally, I know we all hate mouse, but sometimes it's useful, so let's enable mouse clicks:

```
set mouse=a
```

In the next post, we can go over more settings as well as understanding viml, vim's scripting language.
