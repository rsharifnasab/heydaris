---
title: Deep Learning and Machine Learning - an Overview
date: 2020-06-12 23:59:17 -06:00
categories:
path: learning
---

The idea of Artificial Intelligence as portrayed in science fiction, was so interesting to me that I always used to daydream about having a smart computer doing what I asked it to do. In fact, that was one of the main reasons I started learning programming and studied computer science in university. But I soon found out that classical AI is not what was promised to us, it's just search algorithms in slightly different scenarios. But at the same time, I found something much more interesting, machine learning and deep learning. You see, although technically speaking deep learning is a subset of machine learning, which itself is a subset of artificial intelligence, the section known as ML is way more powerful than the classical AI stuff. 

ML and DL allow the system to observe what it is supposed to do and then learn to do it on its own. You don't need to program it to do every small detail, but at the same time, it becomes much harder to manage. The structure of the algorithm, the gathering/cleaning/preprocessing of the data, and so much more effects your program that sometimes it's easier to just code the damn thing yourself instead of relying on ML, yet, when you have enough resources and you can actually do it, it usually does a better job than any other system.

So, how does it work?

In this post and similar upcoming ones, I'm hoping to cover a few starting points and common pitfalls of ML and DL. The things that either helped me a lot after I learned them or the things that I struggled with a lot. These might not be the complex stuff you are hoping them to be, but they are important nonetheless. So without further ado, let's dive in.

# Objective

So, what does an ML algorithm, including deep learning, do? Well that depends, but typically it's a function that takes inputs and produces outputs, and depending on what these are, it's one of the following things (or a mixture of them):

## Categorizing

In this scenario, the function f decides which category the inputs belong to. Common examples are image classification where the algorithm decides if it is seeing a cat or a dog. Of course, it could be more complex and categorize hundreds of other things at once as well. Another example would be spam detection, where given an email, the algorithm has to decide if it is a spam or not.

## Regression

Regression algorithms try to assign a numerical value to the data, for example, estimate the price of a house given the description of it, like how many rooms and bathrooms it has or what part of the city it is in. 

## Generation

Where the algorithm tries to create new things based on what it has seen previously, like the poem generation or the face generation that usually make the news.

## Clustering

Clustering is somewhat similar to categorizing, but instead of assigning each item to predefined categories, we cluster data that look similar together. This helps us to identify users with similar behavior or find an irregular banking activity that helps to protect your identity.


## Control

A control algorithm is exactly what it sounds like, it controls a system, such as a robot, and decides what it should do next.

Of course, these are not the only categories and there are many more, but it's a start. Next, let's look at the type of training data we have:

# Training Type

Another useful categorization of algorithms is based on their training data, that is, what do they need to be trained?

## Supervised Learning

Supervised algorithms require pairs of input/expected output to be trained. These are usually the classification or regression methods mentioned above. They are a great place to start learning about machine learning, as they can give you a base idea of how stuff works without being too complex.

## Unsupervised Learning

Unsupervised learning is the exact opposite of supervised learning, they only require input, the machine doesn't require any labels or outputs, it will learn solely based on the input type. Typically generating and clustering fall in this category.

## Semi-Supervised Learning

The idea behind semi-supervised learning is simple, we take what we love about the previous approaches and... throw them away. So instead of having expected output associated with every input, we do that only for a small subset of our data and leave the rest unlabeled. Then the algorithm uses both of them to improve their own behavior.

## Reinforcement Learning

Reinforcement learning doesn't have input and expected output, instead, we have inputs and a grader. Basically, after each output, the grader looks at the results and gives you a grade. For example, when training a walking robot, the grader might consider how human-like it was, how fast it was, and how many times it fell on the ground. Then the system learns to get the best possible grade, therefore learning to walk better. Reinforcement learning is used in many places, such as control algorithms mentioned above.

So, we have decided on the type of our output, and the kind of input we have, then we need to look at what a model actually is.

# Model

A model in ML is like an application in programming. It has an algorithm and is possibly trained on a dataset. From outside, we can look at a model as a black-box function, that takes the input and produces output, we don't know how it does it, and we don't need to know.

But there is one thing we need to know about the model, how well does it behave? We can look into that question and a few other similar things in the next post!
