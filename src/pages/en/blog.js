import React from "react";
import {graphql} from "gatsby";

import Layout from "../../components/layout";
import PostLink from "../../components/post_link";
import PostList from "../../components/post_list";

export default ({data}) => (
  <Layout>
    <h2>Posts</h2>
    <PostList>
      {data.allMarkdownRemark.edges.map(({node})=>(
        <PostLink link={"/en/blog/"+node.frontmatter.path} date={node.frontmatter.date}>
          {node.frontmatter.title}
        </PostLink>
      ))}
    </PostList>
  </Layout>
);

export const query = graphql`
    {
      allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/blog_en/"}}, sort: {order: DESC, fields: frontmatter___date}) {
        edges {
          node {
            frontmatter {
              path
              title
              date(formatString: "MMMM Do, Y")
            }
          }
        }
      }
    }
`;
