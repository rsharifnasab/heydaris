---
title: vim
path: vim
---

ادیتور ویم قابلیت‌هایی به شما میده که باعث افزایش سرعت شما میشه. این ادیتور علاوه بر این که روی همه سیستم‌عامل‌ها به صورت پیش‌فرض نصب هست به شما اجازه میده که تنظیمات زیادی داشته باشید. 

ویدیوهای من درباره ویم:

* [نگاهی به ویم](https://youtu.be/gojr3jZqFX0)
* [کانفیگ ویم](https://youtu.be/vHB9FdsPbFs)

تنظیمات من: https://github.com/MCSH/dot-files

*هشدار* هرگز محتویات کانفیگ کسی رو بدون فهمیدنش کپی نکنید.

پست‌های مرتبط من:

* [neovim](https://heydaris.com/fa/neovim)
* [neovim complete](https://heydaris.com/fa/neovim-complete)

قبل از هر چیزی neovim رو برای سیستم‌عامل‌تون نصب کنید

https://github.com/neovim/neovim/wiki/Installing-Neovim

دقت کنید که دو پکیج وجود داره،‌ پکیج خود نئوویم و پکیج پایتون همراهش.


